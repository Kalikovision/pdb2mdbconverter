﻿using Pdb2Mdb;
using System.IO;
using System.Linq;

namespace Pdb2MdbConverter
{
    class Program
    {
        /// <summary>
        /// </summary>
        /// <param name="args">
        /// 0 - inputfile
        /// </param>
        static void Main(string[] args)
        {
            if (args != null && args.Length >= 1)
            {
                string inputFile = args[0];
                Converter.Convert(inputFile);

                //Leaving this for future me. Sometimes when you do a conversion, it just adds .mdb to the .dll so it looks dumb and doesn't work
                //This was supposed to rename that, but I'm having all sorts of issues with other things and can't confirm this is working. 
                //Sorry future me. Good luck o7

                //var files = Directory.GetFiles(Path.GetDirectoryName(inputFile));

                //if (files == null) return;
                //string wrongExt = ".dll.mdb";

                ////There is something weird about hte converter that keeps the .dll in it, so nuke it
                //foreach (var file in files)
                //{
                //    if (file.StartsWith(inputFile) && file.EndsWith(wrongExt))
                //    {
                //        string newFileName = inputFile.Replace(".dll", "");
                //        if (File.Exists(newFileName)) File.Delete(newFileName);

                //        File.Move(file, newFileName);
                //        File.Delete(file);
                //        break;
                //    }
                //}

            }
        }
    }
}
