This will create a .exe that takes a single argument to a .dll (must contain a matching .pdb file) and generate a Unity .mdb file at the same location

Can use with VS Post build events as such:

if "$(SolutionName)" == "UnityGame" "$(SolutionDir)\Pdb2MdbConverter.exe" "$(SolutionDir)Assets\ProjectDLLS\$(ProjectName).dll" /y


ProjectDLLs is where I happen to copy all my dlls to after building them when in my "UnityGame" solution

Might also consider using the if $(ConfigurationName) == Debug check as well since that is the only time it is relevant


UNRELATED, BUT USEFUL:

Copying the pdb file for Unity: (wrap in debug check as well)
if "$(SolutionName)" == "UnityGame" copy "$(TargetDir)$(TargetName).pdb" "$(SolutionDir)Assets\ProjectDLLs"

Copying the .dll to an output directory for Unity
if "$(SolutionName)" == "UnityGame" copy "$(TargetPath)" "$(SolutionDir)Assets\ProjectDLLs"